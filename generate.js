const { deepStrictEqual } = require('assert');
const fs = require('fs');
const path = require('path');

// config

const sourceFolder = 'source';
const convertedFolder = 'converted';

// functions

function rmdirSync(dir) {
    var list = fs.readdirSync(dir);
    for (var i = 0; i < list.length; i++) {
        var filename = path.join(dir, list[i]);
        var stat = fs.statSync(filename);
        if (filename == '.' || filename == '..') {
        } else if (stat.isDirectory()) {
            rmdirSync(filename);
        } else {
            fs.unlinkSync(filename);
        }
    }
    fs.rmdirSync(dir);
}

function createImgFolder(folder, sceneName, imgFile) {
    const folderPath = path.join(convertedFolder, folder, sceneName);
    fs.mkdirSync(folderPath);
    fs.copyFileSync(path.join(sourceFolder, folder, imgFile), path.join(convertedFolder, folder, sceneName, 'img.png'));
}

// script start

fs.readdirSync(sourceFolder).forEach(file => {

    const subPath = path.join(sourceFolder, file);
    if (!fs.lstatSync(subPath).isDirectory() || file.endsWith('.prs_unzip'))
        return;

    const pngFiles = [];
    fs.readdirSync(subPath).forEach(subFile => {
        if (subFile.endsWith('.png'))
            pngFiles.push(subFile);

    });

    const targetPath = path.join(convertedFolder, file);

    if (fs.existsSync(targetPath)) {
        rmdirSync(targetPath);
    }
    fs.mkdirSync(targetPath);

    console.log('Creating files for ' + file + '...');

    const lessonJSONPath = path.join(subPath, 'lesson.json');
    var lessons = [];
    if (fs.existsSync(lessonJSONPath)) {
        const json = JSON.parse(fs.readFileSync(lessonJSONPath));
        for (var key in json.scenes)
            for (var innerKey in json.scenes[key])
                lessons.push(innerKey);
    }

    if (lessons.length > 0)
        console.log('Using lesson.json scene names (' + lessons.length + ' Scenes)...');

    var index = 0;
    var sceneName = null;

    pngFiles.sort().forEach(pngFile => {
        index++;
        if(lessons.length == 0 || lessons.length > index -1) {
            sceneName = (lessons.length > 0) ? lessons[index - 1] : (index < 10) ? 'SC_Slide0' + index : 'SC_Slide' + index;    
            createImgFolder(file, sceneName, pngFile);
            console.log('Created ' + sceneName + '/img.png');
        }
    });

    if (lessons.length > 0 && (lessons.length - pngFiles.length) != 0)
        console.log('WARNING:\nlesson.json scenes count: ' + lessons.length + '\npng files count: ' + pngFiles.length);

});