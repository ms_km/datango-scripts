const fs = require('fs');
const path = require('path');
const AdmZip = require('adm-zip');

// config

const sourceFolder = 'source';
const convertedFolder = 'converted';

// functions

function rmdirSync(dir) {
    var list = fs.readdirSync(dir);
    for (var i = 0; i < list.length; i++) {
        var filename = path.join(dir, list[i]);
        var stat = fs.statSync(filename);
        if (filename == '.' || filename == '..') {
        } else if (stat.isDirectory()) {
            rmdirSync(filename);
        } else {
            fs.unlinkSync(filename);
        }
    }
    fs.rmdirSync(dir);
}

function extractFile(sourcePath, toPath) {
    var zip = new AdmZip(sourcePath);
    zip.extractAllTo(toPath, true);
}

function writeZipFile(compressPath, zipPath) {
    var zip = new AdmZip();
    zip.addLocalFolder(compressPath);
    zip.writeZip(zipPath);
}

function replaceManifestFile(filePath) {
    var fileStr = fs.readFileSync(filePath, 'utf-8');
    fileStr = fileStr.replace('<manifest xmlns=', '<manifest xmlns:imsss=');
    fs.writeFileSync(filePath, fileStr, { flag: 'w' });
}

// script start

fs.readdirSync(sourceFolder).forEach(file => {
    if (!file.endsWith('.zip'))
        return;

    const filePath = path.join(sourceFolder, file);
    const unzipPath = filePath + '_unzip';
    console.log('Extracting ' + filePath + '...');
    extractFile(filePath, unzipPath);
    console.log('Extracting done.');

    const manifestFilePath = path.join(unzipPath, 'imsmanifest.xml');
    replaceManifestFile(manifestFilePath);
    console.log('manifest file replaced.');

    writeZipFile(unzipPath, path.join(convertedFolder, file));
    rmdirSync(unzipPath);
    console.log('Converted file: ' + convertedFolder + '/' + file);
});