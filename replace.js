const fs = require('fs');
const path = require('path');
const AdmZip = require('adm-zip');

// config

const sourceFolder = 'source';
const convertedFolder = 'converted';

// functions

function rmdirSync(dir) {
    var list = fs.readdirSync(dir);
    for (var i = 0; i < list.length; i++) {
        var filename = path.join(dir, list[i]);
        var stat = fs.statSync(filename);
        if (filename == '.' || filename == '..') {
        } else if (stat.isDirectory()) {
            rmdirSync(filename);
        } else {
            fs.unlinkSync(filename);
        }
    }
    fs.rmdirSync(dir);
}

function extractFile(sourcePath, toPath) {
    var zip = new AdmZip(sourcePath);
    zip.extractAllTo(toPath, true);
}

function writeZipFile(compressPath, zipPath) {
    var zip = new AdmZip();
    zip.addLocalFolder(compressPath);
    zip.writeZip(zipPath);
}

function recursiveReplace(fromObj, toObj, objKey) {
    var toKeyObj = toObj[objKey];
    if (typeof toKeyObj === 'object' && toKeyObj !== null) {
        for (var key in toKeyObj) {
            if (fromObj[objKey]) {
                recursiveReplace(fromObj[objKey], toKeyObj, key);
            }
        }
    }
    else {
        fromObj[objKey] = toKeyObj;
    }
}

function replaceLessonFile(filePath) {
    var lessonStr = fs.readFileSync(filePath);
    var lessonObj = JSON.parse(lessonStr);
    lessonObj.scenes.forEach(scene => {
        for (var sceneKey in scene) {
            var sceneData = scene[sceneKey];
            sceneData.actions.forEach(actionObj => {
                for (var replaceKey in replacementsJSON) {
                    if (actionObj[replaceKey])
                        recursiveReplace(actionObj, replacementsJSON, replaceKey);
                }
            });
        }
    });
    const jsonStr = JSON.stringify(lessonObj);
    fs.writeFileSync(filePath, jsonStr, { flag: 'w' });
}

// script start

const replacementsJSON = JSON.parse(fs.readFileSync('replacements.json'));

fs.readdirSync(sourceFolder).forEach(file => {
    if (!file.endsWith('.prs'))
        return;

    const filePath = path.join(sourceFolder, file);
    const unzipPath = filePath + '_unzip';
    console.log('Extracting ' + filePath + '...');
    extractFile(filePath, unzipPath);
    console.log('Extracting done.');

    const lessonPath = path.join(unzipPath, 'lesson');
    fs.readdirSync(lessonPath).forEach(folderCode => {
        console.log('Replacing values...');
        const lessonJSONFile = path.join(lessonPath, folderCode, 'lesson.json');
        replaceLessonFile(lessonJSONFile);
        console.log(lessonJSONFile.substring(lessonJSONFile.indexOf('lesson') + 6) + ' values replaced.');
    });

    writeZipFile(unzipPath, path.join(convertedFolder, file));
    rmdirSync(unzipPath);
    console.log('Converted file: ' + convertedFolder + '/' + file);
});